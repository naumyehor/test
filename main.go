package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"os"

	"github.com/BurntSushi/toml"
)

type constitution struct {
	Meta       meta   `toml:"meta_info"`
	Parameters Params `toml:"parameters"`
}

type Params struct {
	TxFee struct {
	}
	Distribution distribution
}

type meta struct {
	Title                    string `toml:"title"`
	QConstitutionVersionHash string `toml:"Q_Constitution_Version_Hash"`
}

type distribution struct {
	QTokenHoldersTransactionFeeShare   float32 `toml:"Q_Token_Holders_Transaction_Fee_Share"`
	QTokenHoldersNativeAppsFeeShare    float32 `toml:"Q_Token_Holders_Native_Apps_Fee_Share"`
	QTokenHoldersCoinbaseSubsidyShare  float32 `toml:"Q_Token_Holders_Coinbase_Subsidy_Share"`
	QIDHoldersTransactionFeeShare      float32 `toml:"QID_Holders_Transaction_Fee_Share"`
	QIDHoldersNativeAppsFeeShare       float32 `toml:"QID_Holders_Native_Apps_Fee_Share"`
	QIDHoldersCoinbaseSubsidyShare     float32 `toml:"QID_Holders_Coinbase_Subsidy_Share"`
	ValidatorNodesTransactionFeeShare  float32 `toml:"Validator_Nodes_Transaction_Fee_Share"`
	ValidatorNodesNativeAppsFeeShare   float32 `toml:"Validator_Nodes_Native_Apps_Fee_Share"`
	ValidatorNodesCoinbaseSubsidyShare float32 `toml:"Validator_Nodes_Coinbase_Subsidy_Share"`
	RootNodesTransactionFeeShare       float32 `toml:"Root_Nodes_Transaction_Fee_Share"`
	RootNodesNativeAppsFeeShare        float32 `toml:"Root_Nodes_Native_Apps_Fee_Share"`
	RootNodesCoinbaseSubsidyShare      float32 `toml:"Root_Nodes_Coinbase_Subsidy_Share"`
}

func main() {
	htmlTemplate, err := ioutil.ReadFile("static/template.html")
	if err != nil {
		log.Fatalln(err)
	}

	tomlData, err := ioutil.ReadFile("constitution.toml")
	if err != nil {
		log.Fatalln(err)
	}

	var constToml constitution
	if _, err := toml.Decode(string(tomlData), &constToml); err != nil {
		log.Fatalln(err)
	}

	fmt.Println(constToml)

	tmpl, err := template.New("constitution").Parse(string(htmlTemplate))
	if err != nil {
		log.Fatalln(err)
	}

	file, err := os.OpenFile("public/index.html", os.O_CREATE|os.O_WRONLY, os.ModePerm)
	if err != nil {
		log.Fatalln(err)
	}

	err = tmpl.Execute(file, constToml)
	if err != nil {
		log.Fatalln(err)
	}
}
